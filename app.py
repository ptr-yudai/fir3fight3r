#!/usr/bin/env python
from core.firefighter import FireFighter
import os
import sys
import json

def load_config():
    with open("config.json", "r") as f:
        config = json.load(f)
    
    if 'iface' not in config:
        raise SyntaxError("'iface' does not exist in `config.json`")
    if 'path' not in config:
        raise SyntaxError("'path' does not exist in `config.json`")
    for key in ['plugin', 'database', 'rule']:
        if key not in config['path']:
            raise SyntaxError("'path.{}' does not exist in `config.json`".format(key))
    return config

if __name__ == '__main__':
    euid = os.geteuid()
    if euid != 0:
        args = ['sudo', sys.executable] + sys.argv + [os.environ]
        os.execlpe('sudo', *args)

    conf = load_config()
    
    app = FireFighter(
        plugindir = conf['path']['plugin'],
        dbname = conf['path']['database'],
        rulename = conf['path']['rule'],
        unique_span = conf['span']
    )
    if 'whitelist' in conf:
        for addr in conf['whitelist']:
            app.add_whitelist(addr)
    
    app.dispatch(
        iface = conf['iface']
    )
