"""DoS Attack Detector

Detects IP addresses from which massive traffics are coming.
"""
from Plugin import Plugin
from scapy.all import *
import json
import sqlite3
import datetime
import random

class DosDetector(Plugin):
    def __init__(self):
        # Connect to DB
        self.db = sqlite3.connect(
            'plgdata/dos.db',
            detect_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
        )
        sqlite3.dbapi2.converters['DATETIME'] = sqlite3.dbapi2.converters['TIMESTAMP']
        self.table_created = False
        return
    
    def filter(self, packet, args):
        self.span = args['span']
        self.maximum = args['maximum']
        self.port = args['port']
        
        if IP not in packet: return False
        if TCP not in packet[IP]: return False
        if packet[IP][TCP].dport != self.port and packet[IP][TCP].sport != self.port:
            return False

        if not self.table_created:
            self._create_table()

        self.record_ip(packet[IP].src)
        if self.check_ip(packet[IP].src):
            return True
        else:
            return False

    def check_ip(self, address):
        c = self.db.cursor()

        c.execute(
            '''
            SELECT id, access_at FROM log_{} WHERE address=?;
            '''.format(self.port),
            (address,)
        )

        # Count up recent accesses
        count = 0
        old_log = []
        base = datetime.datetime.now() - datetime.timedelta(seconds=self.span)
        for row in c.fetchall():
            if row[1] > base:
                count += 1
            else:
                old_log.append((row[0],))

        # Delete old logs
        if old_log:
            c.executemany(
                '''
                DELETE FROM log_{} WHERE id=?;
                '''.format(self.port),
                old_log
            )

        if count > self.maximum:
            return True

        self.db.commit()
        c.close()
        return False
        
    def record_ip(self, address):
        c = self.db.cursor()

        c.execute(
            '''
            INSERT INTO log_{}(address, access_at)
            VALUES(?, ?);
            '''.format(self.port),
            (address, datetime.datetime.now())
        )

        self.db.commit()
        c.close()
        return

    def _create_table(self):
        c = self.db.cursor()
        
        c.execute('''CREATE TABLE IF NOT EXISTS log_{} (
        id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        address     TEXT    NOT NULL,
        access_at   DATETIME
        );'''.format(self.port))

        self.db.commit()
        c.close()
        return

    def __del__(self):
        self.db.close()
