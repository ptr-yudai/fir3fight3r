"""Slack Notify Plugin

Notify a suspicious event through slack webhook
"""
from Plugin import Plugin
from scapy.all import *
import requests
import json

class SlackNotify(Plugin):
    def __init__(self):
        with open("plgdata/webhook.json", "r") as f:
            webhook = json.load(f)
        self.url = webhook['url']
        self.icon = webhook['icon']
        self.name = webhook['name']
        print("[+] SlackNotify: webhook='{}'".format(self.url))
        return
    
    def event(self, packet, db, rule):
        address = packet[IP].src if IP in packet else 'UNKNOWN'
        text = "Detected suspicious packet from *{}*".format(address)
        cc = self._ip2country(address)
        if cc:
            text += " ({} :flag-{}:)".format(cc[0], cc[1])
        if 'message' in rule:
            text += "\nMessage: *{}*".format(rule['message'])
        payload = {
            "username": self.name,
            "icon_emoji": self.icon,
            "text": text
        }
        try:
            r = requests.post(self.url, data=json.dumps(payload))
        except:
            print("[-] SlackNotify: Failed to post payload")
        return

    def _ip2country(self, address):
        try:
            r = requests.get("http://ip-api.com/json/{}".format(address))
            result = json.loads(r.text.encode("ascii"))
        except:
            return None
        if result['status'] == 'success':
            return result['country'], result['countryCode'].lower()
        else:
            return None
