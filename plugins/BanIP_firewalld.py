"""Temporary Block IP

Add an IP to blacklist by using firewalld
"""
from Plugin import Plugin
from scapy.all import *
import subprocess
import json

class BanIP_firewalld(Plugin):
    def __init__(self):
        with open("plgdata/banip.json", "r") as f:
            conf = json.load(f)
        if 'timeout' not in conf:
            raise SyntaxError("'timeout' does not exist in `banip.json`")
        self.timeout = conf['timeout']

        try:
            p = subprocess.Popen(["/usr/bin/which", "firewall-cmd"], stdout=subprocess.PIPE, close_fds=True)
        except:
            raise NotImplementedError("[-] firewalld is not installed!")
            
        if p.wait() == 1:
            raise NotImplementedError("[-] firewalld is not installed!")
        
        return
    
    def event(self, packet, db, rule):
        if IP not in packet: return
        address = packet[IP].src
        
        if self.timeout < 0:
            # Permanent
            p = subprocess.Popen(
                [
                    "firewall-cmd",
                    "--zone=drop",
                    "--add-source={}".format(address),
                    "--permanent"
                ],
                close_fds=True,
                stdout=subprocess.PIPE
            )
        else:
            # Temporary
            p = subprocess.Popen(
                [
                    "firewall-cmd",
                    "--zone=drop",
                    "--add-source={}".format(address),
                    "--timeout={}".format(self.timeout)
                ],
                close_fds=True,
                stdout=subprocess.PIPE
            )
        p.wait()
        
        p = subprocess.Popen(["firewall-cmd", "--reload"],
                             stdout=subprocess.PIPE,
                             close_fds=True)
        p.wait()
        return
