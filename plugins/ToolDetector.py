"""Web Tool Detector

Detects HTTP traffic whose user agent is suspicious.
"""
from Plugin import Plugin
from scapy.all import *
import re

class ToolDetector(Plugin):
    UA_BLACKLIST = [
        'dirbuster',
        'sqlmap',
        'fimap',
        'nmap',
        'netcrawler',
        'profound',
        'scrapyproject',
        'nikto',
        'jersey',
        'brandwatch',
        'magpie-crawler',
        'mechanize',
        'python-requests',
        'redback'
    ]
    
    def filter(self, packet, args):
        if IP not in packet: return False
        if TCP not in packet[IP]: return False
        if packet[IP][TCP].dport != 80 and packet[IP][TCP].sport != 80:
            return False
        
        headers = packet.sprintf("{Raw:%Raw.load%}").split(r"\r\n")
        for header in headers:
            r = re.findall("User-Agent: (.+)", header)
            if not r: continue
            if self.check_ua(r[0]):
                return True
            
        return False

    def check_ua(self, ua):
        for f in self.UA_BLACKLIST:
            if f in ua.lower():
                return True
        return False
