class Plugin(object):
    def filter(self, packet, args):
        raise NotImplementedError
    
    def event(self, packet, db, rule):
        raise NotImplementedError

