# Fir3Fight3r
A simple and strong firewall tool.

## 0. Requirements
Scapy is required.
```
pip install scapy
```

This tool works both on Python 2.7.x and Python 3.x. (It depends on plugins.)

## 1. How to setup?
Download this repository to anywhere you want to.
```
git clone https://bitbucket.com/ptr-yudai/Fir3Fight3r.git
cd ./Fir3Fight3r
```
After you cloned the repository, you must change `rules.json` and `config.json` first.
See `3. Rule` to understand how to write `rules.json`.
You can set the following values in `config.json`.

| key | value | Description |
|:-:|:-:|:-:|
| iface | Interface name (str) | Set an interface name to listen packets on. |
| path | plugin/database/rule path config (list of dict) | Set path and filename. You don't need to change these fields. |
| span | Span to check duplicate rule in (int) | If a rule hit for 2 packets with same IP and it's in the span, the second one will be ignored. |
| whitelist | IP list to ignore (list of str) | Every IP address written in this whitelist will be ignored. |

## 2. How to run?
Simply run
```
$ ./app.py
```
Although this tool requires root privilege, you don't need to be root as it runs `sudo`. (Especially in case you use pyenv.)

## 3. Rule
`rules.json` is a JSON-style text file. The json must be a list of your rules. Each rule is a dictionary which must have `rule` and `event` keys, and may have `message` and `log` keys optionally. For example, the following `rules.json` has 2 rules.

```json
[
  {
    "rule": [{"filter": "ip", "value": "192.168.1.1"}],
    "event": ["allow"]
  },
  {
    "rule": [{"filter": "script", "value": "plugin.ToolDetector"}],
    "event": ["plugin.BanIP_firewalld", "plugin.SlackNotify"],
    "message": "Detected web tool!"
  }
]
```

The first rule means every packet from `192.168.1.1` will be allowed.
The second rule means every packet which applies to ToolDetector plugin causes BanIP_firewalld and SlackNotify events, and the message will be recorded in event log.
See `4. Plugin` to understand how to write plugins.
The following plugins are provided in default.

| plugin | attribute | Description |
|:-:|:-:|
| SlackNotify | event | This plugin sends the IP address and message to slack. You have to configure `plgdata/webhook.json`. |
| DosDetector | rule | This plugin records IP packets and detects DoS attack. You can specify port and maximum connection per span. |
| ToolDetector | rule | This plugin detects HTTP requests whose User-Agent is suspicious. |

### 4. Plugin
Every plugin must be put in `plugins` directory.
A plugin is written in Python and each script must have a class whose name is same as that of its filename.
The class must inherit a parent class `Plugin` (defined in `plugins/Plugin.py`).
A plugin need to define either `filter` or `event` method.

`filter` is called when filter is `script` and value is the plugin name in a rule.
`filter` method receives `packet` and `args` as arguments.
`packet` is a packet of scapy Packet class.
`args` is a dictionary defined as `args` in a rule.

`event` is called when the rule is applied to a packet.
`event` method receives `packet`, `db` and `rule` as arguments.
`packet` is the packet which violated the rule.
`db` is a database connection to sqlite3.
`rule` is the violated rule (dictionary) from `rules.json`.

It's recommended to use `plgdata` directory when you want to keep some files or databases for your plugin.
