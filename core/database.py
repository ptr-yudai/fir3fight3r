import sqlite3
import datetime

class Database(object):
    """
    
    """
    def __init__(self, name="database.db"):
        self.db = sqlite3.connect(
            name,
            detect_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
        )
        sqlite3.dbapi2.converters['DATETIME'] = sqlite3.dbapi2.converters['TIMESTAMP']
        self.setup_tables()
        return

    def update_ip(self, address):
        """
        Update IP table

        Parameters
        ----------
        address:
            IP address to update. If the IP doesn't exist in IP table, the IP is added to the table. Otherwise, count column is incremented and last_access is updated.
        """
        c = self.db.cursor()

        c.execute("SELECT id FROM ip WHERE address=?", (address,))
        if c.fetchone() is None:
            # Add IP to the table
            data = (
                address,
                datetime.datetime.now(),
                datetime.datetime.now()
            )
            c.execute(
                '''
                INSERT INTO
                  ip(address, created_at, last_access)
                  VALUES(?, ?, ?)
                ''',
                data
            )
        else:
            # Update count and last_access
            data = (
                datetime.datetime.now(),
                address
            )
            c.execute(
                '''
                UPDATE ip SET count=count+1, last_access=? WHERE address=?
                ''',
                data
            )

        self.db.commit()
        c.close()
        return

    def unique_event(self, rule, address, message, span):
        """
        Check if the same event is recorded past

        Parameters
        ----------
        rule: dict
            Matching rule.
        address: str
            Target IP address.
        message: str
            Event information
        span: int
            Maximum span to survey (in seconds)
        """
        c = self.db.cursor()
        
        filter_name = self._construct_filter_name(rule)

        data = (filter_name, address, message)
        c.execute(
            '''
            SELECT timestamp FROM event
            WHERE filter=? AND address=? AND message=?
            ''',
            data
        )

        flag = True
        for row in c.fetchall():
            if row[0] > datetime.datetime.now() - datetime.timedelta(seconds=span):
                flag = False
                break

        c.close()
        
        return flag

    def record_event(self, rule, address, message):
        """
        Record an event

        Parameters
        ----------
        rule: dict
            Matching rule.
        address: str
            Target IP address.
        message: str
            Event information.
        """
        c = self.db.cursor()

        filter_name = self._construct_filter_name(rule)

        data = (filter_name, address, message, datetime.datetime.now())
        c.execute(
            '''
            INSERT INTO
            event(filter, address, message, timestamp)
            VALUES(?, ?, ?, ?)
            ''',
            data
        )
        print("[+] New event:")
        print(" | filter = {}".format(filter_name))
        print(" | address = {}".format(address))
        print(" | message = {}".format(message))
        print(" | timestamp = {}".format(datetime.datetime.now()))

        self.db.commit()
        c.close()
        return

    def setup_tables(self):
        """
        Create tables if not exists
        """
        c = self.db.cursor()
        
        # 'ip' table
        c.execute('''CREATE TABLE IF NOT EXISTS ip (
        id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        address     TEXT    NOT NULL UNIQUE,
        count       INTEGER NOT NULL DEFAULT 1,
        created_at  DATETIME,
        last_access DATETIME
        );''')

        # 'event' table
        c.execute('''CREATE TABLE IF NOT EXISTS event (
        id        INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
        filter    TEXT     NOT NULL,
        address   TEXT     NOT NULL,
        message   TEXT     NOT NULL,
        timestamp DATETIME NOT NULL
        );''')
        
        self.db.commit()
        c.close()
        return

    def _construct_filter_name(self, rule):
        filter_name = ''
        for r in rule:
            filter_name += '({},{}), '.format(r['filter'], r['value'])
        else:
            filter_name = filter_name[:-2]
        return filter_name
    
    def __del__(self):
        self.db.close()
