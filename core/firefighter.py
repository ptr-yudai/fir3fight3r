from scapy.all import *
import core.database
import core.judge
import core.statesman

class FireFighter(object):
    """
    Fir3Fight3r detects DDoS and blocks the source IP
    """
    EVENT_TABLE = {}
    
    def __init__(self,
                 plugindir='plugins',
                 dbname='database.db',
                 rulename='rules.json',
                 unique_span=600):
        self.unique_span = unique_span
        self.whitelist = []
        self.statesman = core.statesman.Statesman(name=plugindir)
        self.court = core.judge.Judge(self.statesman, name=rulename)
        self.db = core.database.Database(name=dbname)
        self._add_default_event()
        self._validate_rule()
        return

    def add_whitelist(self, address):
        """
        Add an IP which is not written in IP log

        Parameters
        ----------
        address: str
            IP address to ignore.
        """
        self.whitelist.append(address)
    
    def dispatch(self, filter="", iface="any"):
        """
        Start sniffing

        Parameters
        ----------
        filter: str
            BPF filter for packets to capture.
        iface: str
            The interface name to capture packets on.
        """
        print("[+] Start sniffing...")
        while True:
            sniff(
                filter = filter,
                iface = iface,
                prn = self._callback,
                store = 0,
                count = 100
            )
        return

    def _callback(self, packet):
        """
        Callback function for scapy sniff
        """
        # Update IP log
        if IP in packet:
            if packet[IP].src in self.whitelist:
                return
            else:
                self.db.update_ip(packet[IP].src)
        
        # Judge
        rule = self.court.judge(packet, self.db)
        if rule is None: return

        # Write event log
        address = packet[IP].src if IP in packet else ''
        message = rule['message'] if 'message' in rule else ''
        if self.db.unique_event(rule['rule'], address, message, self.unique_span):
            if 'log' not in rule or rule['log'] == True:
                self.db.record_event(rule['rule'], address, message)

            # Take action
            for evt in rule['event']:
                if evt in self.EVENT_TABLE:
                    # Default event
                    self.EVENT_TABLE[evt](packet, self.db, rule)

                elif evt in self.statesman.PLUGIN_LIST:
                    # Plugin event
                    try:
                        self.statesman.PLUGIN_LIST[evt].event(packet, self.db, rule)
                    except Exception as e:
                        print("[-] callback: {}".format(e))

                else:
                    raise NotImplementedError("Event '{}' not found".format(evt))
        return

    def _add_default_event(self):
        """
        Add default events to event table
        """
        self.EVENT_TABLE['allow'] = lambda p,d,r: True
        return
    
    def _validate_rule(self):
        """
        Check if rules are written in valid format
        """
        if not isinstance(self.court.rules, list):
            raise SyntaxError("Rule validation: Rule is not a list")

        for rule in self.court.rules:
            if "rule" not in rule:
                raise SyntaxError("Rule validation: 'rule' does not exist")
            if "event" not in rule:
                raise SyntaxError("Rule validation: 'event' does not exist")

            if not isinstance(rule['rule'], list):
                raise SyntaxError("Rule validation: Each rule must be a list")
            
            for r in rule["rule"]:
                if "filter" not in r:
                    raise SyntaxError("Rule validation: 'filter' does not exist")
                if "value" not in r:
                    raise SyntaxError("Rule validation: 'value' does not exist")

                if r['filter'] == 'script' and r['value'] not in self.statesman.PLUGIN_LIST:
                    raise NotImplementedError("Rule validation: Plugin '{}' does not exist".format(r['value']))
                elif r['filter'] not in self.court.RULE_TABLE:
                    raise NotImplementedError("Rule validation: Filter '{}' does not exist".format(r['filter']))

            for e in rule['event']:
                if e not in self.EVENT_TABLE and e not in self.statesman.PLUGIN_LIST:
                    raise NotImplementedError("Rule validation: Event '{}' does not exist".format(e))

        print("[+] Loaded {} rules".format(len(self.court.rules)))
        return
