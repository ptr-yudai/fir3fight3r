import os
import sys
import glob

class Statesman(object):
    PLUGIN_LIST = {}
    
    def __init__(self, name='plugins'):
        self._load_plugins(name)
        return
    
    def _load_plugins(self, d='plugins'):
        """
        Load every plugin in 'plugin' directory and add them to plugin list
        """
        d = os.path.join(os.getcwd(), d)
        sys.path.append(d)
        exception_files = ['__pycache__', '__init__.py', 'Plugin.py']
        plugins = [os.path.relpath(x, d) for x in glob.glob(os.path.join(d, '*.py'))]
        plugins = set(plugins) - set(exception_files)
        
        for filepath in plugins:
            name = filepath.replace('.py', '')
            m = __import__(name, globals(), locals(), [name], 0)
            obj = eval('m.{}()'.format(name))
            self.PLUGIN_LIST['plugin.{}'.format(name)] = obj

        print("[+] Loaded {} plugins".format(len(self.PLUGIN_LIST)))
        return
