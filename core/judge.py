import json
from scapy.all import *

class Judge(object):
    """
    """
    RULE_TABLE = {}
    
    def __init__(self, statesman, name="rules.json"):
        with open(name, "r") as f:
            self.rules = json.load(f)
        self._add_default_rules()
        self.statesman = statesman
        return

    def judge(self, packet, db):
        """
        Judge whether the packet is hit by rule

        Parameters
        ----------
        packet: Packet
            A packet to analyse.
        db: Database
            The database that has past events and IPs.
        """
        for rules in self.rules:
            flag = True
            
            for rule in rules['rule']:
                if rule['filter'] in self.RULE_TABLE:
                    # Check for the rule
                    flag = flag and self.RULE_TABLE[rule['filter']](
                        packet,
                        rule['value'],
                        rule['args'] if 'args' in rule else {}
                    )
                else:
                    raise NotImplementedError("Filter '{}' not found".format(rule['filter']))
                    
            if flag:
                return rules # Met constraints
        return None

    def _cmp_ip_src(self, packet, value):
        """ ip.src == :value """
        if IP not in packet: return False
        return packet[IP].src == value

    def _cmp_ip_dst(self, packet, value):
        """ ip.dst == :value """
        if IP not in packet: return False
        return packet[IP].dst == value

    def _cmp_tcp_dport(self, packet, value):
        """ tcp.dport == :value """
        if IP not in packet: return False
        if TCP not in packet[IP]: return False
        return packet[IP][TCP].dport == value
        
    def _cmp_tcp_sport(self, packet, value):
        """ tcp.sport == :value """
        if IP not in packet: return False
        if TCP not in packet[IP]: return False
        return packet[IP][TCP].sport == value

    def _cmp_time_before(self, packet, value):
        """ time <= :value """
        return packet.time <= value

    def _cmp_time_after(self, packet, value):
        """ time >= :value """
        return packet.time >= value

    def _call_plugin(self, packet, value, args):
        if value in self.statesman.PLUGIN_LIST:
            try:
                return self.statesman.PLUGIN_LIST[value].filter(packet, args)
            except Exception as e:
                print("[-] _call_plugin: {}".format(e))
                return False
        else:
            raise NotImplementedError("Plugin '{}' not found".format(value))
        return False
    
    def _add_default_rules(self):
        """
        Add default filters to rule table
        """
        # IP address
        self.RULE_TABLE['ip'] = lambda p,v,a: self._cmp_ip_dst(p, v) or self._cmp_ip_src(p, v)
        self.RULE_TABLE['ip.dst'] = lambda p,v,a: self._cmp_ip_dst(p, v)
        self.RULE_TABLE['ip.src'] = lambda p,v,a: self._cmp_ip_src(p, v)
        
        # TCP port
        self.RULE_TABLE['tcp.port'] = lambda p,v,a: self._cmp_tcp_dport(p, v) or self._cmp_tcp_sport(p, v)
        self.RULE_TABLE['tcp.dport'] = lambda p,v,a: self._cmp_tcp_dport(p, v)
        self.RULE_TABLE['tcp.sport'] = lambda p,v,a: self._cmp_tcp_sport(p, v)

        # Time
        self.RULE_TABLE['time.before'] = lambda p,v,a: self._cmp_time_before(p, v)
        self.RULE_TABLE['time.after'] = lambda p,v,a: self._cmp_time_after(p, v)

        # Plugin
        self.RULE_TABLE['script'] = lambda p,v,a: self._call_plugin(p, v, a)
        
        return

